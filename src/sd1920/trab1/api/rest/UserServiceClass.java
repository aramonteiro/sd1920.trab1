package sd1920.trab1.api.rest;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import java.util.logging.Logger;

import sd1920.trab1.api.User;

public class UserServiceClass implements UserService{
	
	
	private String domain;
	protected static Map<String, User> allUsers;
	private static Logger Log = Logger.getLogger(UserServiceClass.class.getName());
	
	public UserServiceClass(String domain) {
		this.domain = domain;
		allUsers = new HashMap<String, User>();
	}

	@Override
	public String postUser(User user) {
		if(!user.getDomain().equals(domain)) {
			throw new WebApplicationException( Status.FORBIDDEN );
		}
		
		if(user.getName() == null || user.getPwd() == null || user.getDomain() == null) {
			//Log.info("Message was rejected due to lack of recepients.");
			throw new WebApplicationException( Status.CONFLICT );
		}
		
		synchronized (this) {
			allUsers.put(user.getName(), user);
		}
		
		return user.getName() + "@" + domain;
	}

	@Override
	public User getUser(String name, String pwd) {
		authorizeUser(name, pwd, this);
		
		synchronized (this) {
			return allUsers.get(name);
		}
	}

	@Override
	public User updateUser(String name, String pwd, User user) {
		authorizeUser(name, pwd, this);
		
		synchronized (this) {
			allUsers.replace(name, user);
		}
		
		return user;
	}

	@Override
	public User deleteUser(String user, String pwd) {
		authorizeUser(user, pwd, this);
		
		synchronized (this) {
			return allUsers.remove(user);
		}
	}
	
	protected static void authorizeUser(String name, String pwd, Object object) throws WebApplicationException {
		User curUser = null;
		
		synchronized (object) {
			curUser = allUsers.get(name);
		}
		
		if(curUser == null || !curUser.getPwd().equals(pwd)) {
			Log.info("Requested message does not exists.");
			throw new WebApplicationException(Status.CONFLICT);
		}
	}

}
