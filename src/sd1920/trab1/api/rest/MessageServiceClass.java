package sd1920.trab1.api.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import sd1920.trab1.api.Message;
import sd1920.trab1.api.utills.MessageUtills;

public class MessageServiceClass implements MessageService {
	private Random randomNumberGenerator;

	private final Map<Long,Message> allMessages = new HashMap<Long, Message>();
	private final Map<String,Set<Long>> userInboxs = new HashMap<String, Set<Long>>();

	private static Logger Log = Logger.getLogger(MessageServiceClass.class.getName());
	
	public MessageServiceClass() {
		this.randomNumberGenerator = new Random(System.currentTimeMillis());
	}
	

	@Override
	public long postMessage(String pwd, Message msg) {
		Log.info("Received request to register a new message (Sender: " + msg.getSender() + "; Subject: "+msg.getSubject()+")");		
		//This method throws 403 if the sender does not exist or if the pwd is not correct.
		UserServiceClass.authorizeUser(msg.getSender(), pwd, this);
		
		//Check if message is valid, if not return HTTP CONFLICT (409)
		if(msg.getSender() == null || msg.getDestination() == null || msg.getDestination().size() == 0) {
			Log.info("Message was rejected due to lack of recepients.");
			throw new WebApplicationException( Status.CONFLICT );
		}

		long newID = 0;
		
		synchronized (this) {

			//Generate a new id for the message, that is not in use yet
			newID = Math.abs(randomNumberGenerator.nextLong());
			while(allMessages.containsKey(newID)) {
				newID = Math.abs(randomNumberGenerator.nextLong());
			}

			//Add the message to the global list of messages
			allMessages.put(newID, msg);
		}

		Log.info("Created new message with id: " + newID);
		MessageUtills.printMessage(allMessages.get(newID));

		synchronized (this) {
			//Add the message (identifier) to the inbox of each recipient
			for(String recipient: msg.getDestination()) {
				if(!userInboxs.containsKey(recipient)) {
					userInboxs.put(recipient, new HashSet<Long>());
				}
				userInboxs.get(recipient).add(newID);
			}
		}

		//Return the id of the registered message to the client (in the body of a HTTP Response with 200)
		Log.info("Recorded message with identifier: " + newID);
		return newID;
	
	}

	@Override
	public Message getMessage(String user, long mid, String pwd) {
		Log.info("Received request for message with id: " + mid +".");
		
		
		Message m = null;
		
		synchronized (this) {
			m = allMessages.get(mid);
		}
		
		if(m == null) {  //check if message exists	
			Log.info("Requested message does not exists.");
			throw new WebApplicationException( Status.NOT_FOUND ); //if not send HTTP 404 back to client
		}

		//This method throws 403 if the sender does not exist or if the pwd is not correct.
		UserServiceClass.authorizeUser(allMessages.get(mid).getSender(), pwd, this);

		Log.info("Returning requested message to user.");
		return m; //Return message to the client with code HTTP 200
	}

	@Override
	public List<Message> getMessages(String user, String pwd) {
		Log.info("Received request for messages with optional user parameter set to: '" + user + "'");
		List<Message> messages = new ArrayList<Message>();
		if(user == null) {
			Log.info("Collecting all messages in server");
			synchronized (this) {
				messages.addAll(allMessages.values());
			}
			
		} else {
			//This method throws 403 if the sender does not exist or if the pwd is not correct.
			UserServiceClass.authorizeUser(user, pwd, this);
			Log.info("Collecting all messages in server for user " + user);
			synchronized (this) {
				Set<Long> mids = userInboxs.getOrDefault(user, Collections.emptySet());
				for(Long l: mids) { 
					Log.info("Adding messaeg with id: " + l + ".");
					messages.add(allMessages.get(l));
				}
			}
			
		}
		Log.info("Returning message list to user with " + messages.size() + " messages.");
		return messages;
	}

	@Override
	public void removeFromUserInbox(String user, long mid, String pwd) {
		//This method throws 403 if the sender does not exist or if the pwd is not correct.
		UserServiceClass.authorizeUser(user, pwd, this);
				
		Set<Long> userInbox = userInboxs.getOrDefault(user, Collections.emptySet());
		if(userInbox.isEmpty() || !userInbox.remove(mid))
			throw new WebApplicationException( Status.NOT_FOUND );
		
	}

	@Override
	public void deleteMessage(String user, long mid, String pwd) {
		//This method throws 403 if the sender does not exist or if the pwd is not correct.
		UserServiceClass.authorizeUser(user, pwd, this);
		Message message = allMessages.remove(mid);
		if(message != null)
			for(String userAux: message.getDestination())
				userInboxs.get(userAux).remove(mid);
		
	}

}
